﻿using DeliveryApi.Entites;
using DeliveryApi.Helpers;
using DeliveryApi.Interfaces;
using DeliveryApi.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Helpers;
using webservice.Requests;

namespace DeliveryApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService service;

        public UserController(IUserService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var users = await service.GetAll();
            if (users == null)
            {
                return NotFound();
            }

            var userModels = UserHelper.CovertUsers(users);

            return Ok(userModels);
        }

        [HttpGet]
        [Route("GetByID/{userId}")]
        public async Task<IActionResult> GetUserUnderId(String userId)
        {
            var users = await service.GetUnderId(userId);
            if (users == null)
            {
                return NotFound();
            }

            var userModels = UserHelper.CovertUsers(users);

            return Ok(userModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> CreateUser([FromBody] User user)
        {
            
            var users = await service.Create(user);

            return CreatedAtAction(
                 nameof(Get), new { id = user.UserID }, user);
        }

        [HttpPut]
        [Route("Put/{userId}")]
        public async Task<IActionResult> UpdateUser(String userId, [FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userId != user.UserID)
            {
                return BadRequest();
            }

            await service.Update(userId, user);

            return new NoContentResult();
        }

        [HttpPut]
        [Route("ChangePassword/{userId}")]
        public async Task<IActionResult> ChangePassword(String userId, [FromBody] ChangePassword changePassword)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userId != changePassword.UserID)
            {
                return BadRequest();
            }
            await service.changePassword(userId,changePassword);
            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{userId}")]
        public async Task<IActionResult> DeleteUser(String userId)
        {
            var users = await service.Delete(userId);
            if (users == null)
            {
                return NotFound();
            }

            return Ok(users);
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest loginRequest)
        {
            if (loginRequest == null || string.IsNullOrEmpty(loginRequest.Username) || string.IsNullOrEmpty(loginRequest.Password))
            {
                return BadRequest("Missing login details");
            }

            var loginResponse = await service.Login(loginRequest);

            if (loginResponse == null)
            {
                return BadRequest($"Invalid credentials");
            }

            return Ok(loginResponse);
        }
    }
}
