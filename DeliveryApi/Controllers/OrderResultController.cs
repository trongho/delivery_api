﻿using DeliveryApi.Helpers;
using DeliveryApi.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrderResultController : ControllerBase
    {
        private readonly IOrderResultService service;

        public OrderResultController(IOrderResultService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }

            var models = OrderResultHelper.Covert(entrys);

            return Ok(models);
        }
    }
}
