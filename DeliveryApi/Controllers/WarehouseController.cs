﻿using DeliveryApi.Entites;
using DeliveryApi.Helpers;
using DeliveryApi.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WarehouseController : ControllerBase
    {
        private readonly IWarehouseService service;

        public WarehouseController(IWarehouseService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }

            var models = WarehouseHelper.Covert(entrys);

            return Ok(models);
        }

        [HttpGet]
        [Route("GetByID/{warehouseID}")]
        public async Task<IActionResult> GetUnderId(String warehouseID)
        {
            var entrys = await service.GetUnderId(warehouseID);
            if (entrys == null)
            {
                return NotFound();
            }

            var models = WarehouseHelper.Covert(entrys);

            return Ok(models);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] Warehouse warehouse)
        {

            var entrys = await service.Create(warehouse);

            return CreatedAtAction(
                 nameof(Get), new { id = warehouse.WarehouseID }, warehouse);
        }

        [HttpPut]
        [Route("Put/{WarehouseID}")]
        public async Task<IActionResult> Update(String WarehouseID, [FromBody] Warehouse warehouse)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (WarehouseID != warehouse.WarehouseID)
            {
                return BadRequest();
            }

            await service.Update(WarehouseID, warehouse);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{WarehouseID}")]
        public async Task<IActionResult> Delete(String WarehouseID)
        {
            var entrys = await service.Delete(WarehouseID);
            if (entrys == null)
            {
                return NotFound();
            }

            return Ok(entrys);
        }
    }
}
