﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Entites
{
    public class DeliveryDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderResult> OrderResults { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<Locations> Locations { get; set; }
        public DeliveryDbContext(DbContextOptions<DeliveryDbContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().ToTable("User").HasKey(e => e.UserID);

            modelBuilder.Entity<Order>().ToTable("Order").HasKey(e => e.OrderID);
            modelBuilder.Entity<Order>().Property(p => p.OrderFee).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Order>().Property(p => p.OrderValue).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Order>().Property(p => p.Mass).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Order>().Property(p => p.Kilometers).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<OrderResult>().ToTable("OrderResult").HasKey(e => e.ResultID);

            modelBuilder.Entity<Warehouse>().ToTable("Warehouse").HasKey(e => e.WarehouseID);

            modelBuilder.Entity<Locations>().ToTable("Locations").HasNoKey();
        }
    }
}
