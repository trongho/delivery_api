﻿using DeliveryApi.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Interfaces
{
    public interface ILocationsService
    {
        Task<List<Locations>> GetAll();
        Task<List<Locations>> GetUnderId(String id);
    }
}
