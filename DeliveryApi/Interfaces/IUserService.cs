﻿using DeliveryApi.Entites;
using DeliveryApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Requests;
using webservice.Responses;

namespace DeliveryApi.Interfaces
{
    public interface IUserService
    {
        Task<LoginResponse> Login(LoginRequest loginRequest);
        Task<Boolean> changePassword(String id, ChangePassword changePassword);
        Task<List<User>> GetAll();
        Task<List<User>> GetUnderId(String id);
        Task<Boolean> Create(User user);
        Task<Boolean> Update(String id, User user);
        Task<Boolean> Delete(String id);
    }
}
