﻿using DeliveryApi.Entites;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Interfaces
{
    public interface IOrderService
    {
        Task<String> UploadFile(IFormFile file);
        Task<String> DownloadFile(IFormFile file);
        List<Order> Import();
        String Export();
        Task<List<Order>> GetAll();
        Task<List<Order>> GetUnderId(String id);
        Task<Boolean> Create(Order order);
        Task<Boolean> Update(String id,Order order);
        Task<List<Order>> Patch(String id);
        Task<Boolean> Ended(String id);
        Task<Boolean> Cancel(String id, Order order);
        Task<Boolean> Delete(String id);
        Task<String> GetLastID();
    }
}
