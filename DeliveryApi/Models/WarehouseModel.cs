﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Models
{
    public class WarehouseModel
    {
        public String WarehouseID { get; set; }
        public String WarehouseName { get; set; }
    }
}
