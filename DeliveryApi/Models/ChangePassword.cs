﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Models
{
    public class ChangePassword
    {
        public String UserID { get; set; }
        public String oldPassword { get; set; }
        public String newPassword { get; set; }
        public String reNewPassword { get; set; }
    }
}
