﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Models
{
    public class OrderResultModel
    {
        public String ResultID { get; set; }
        public String ResultName { get; set; }
    }
}
