﻿using DeliveryApi.Entites;
using DeliveryApi.Interfaces;
using DeliveryApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Helpers;
using webservice.Requests;
using webservice.Responses;

namespace DeliveryApi.Services
{
    public class UserService : IUserService
    {
        private readonly DeliveryDbContext context;

        public UserService(DeliveryDbContext context)
        {
            this.context = context;
        }

        public async Task<bool> changePassword(string id, ChangePassword changePassword)
        {
            var entry = context.Users.SingleOrDefault(user => user.Active && user.UserID == changePassword.UserID);
            entry.PasswordSalt = "MTIzNDU2Nzg5MTIzNDU2Nw==";
            entry.Password = HashingHelper.HashUsingPbkdf2(changePassword.newPassword, entry.PasswordSalt);
            context.Entry(entry).CurrentValues.SetValues(entry);
            context.SaveChanges();
            return true;
        }

        public async Task<bool> Create(User user)
        {
            user.PasswordSalt = "MTIzNDU2Nzg5MTIzNDU2Nw==";
            user.Password = HashingHelper.HashUsingPbkdf2(user.Password, user.PasswordSalt);
            var users = context.Users.AddAsync(user);
            context.SaveChanges();
            return users.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var users = context.Users.FirstOrDefault(o => o.UserID.Equals(id));
            context.Users.Remove(users);
            context.SaveChanges();
            return true;
        }

        public async Task<List<User>> GetAll()
        {
            var users = context.Users;
            return await users.ToListAsync();
        }

        public async Task<List<User>> GetUnderId(string id)
        {
            var user = context.Users.Where(u => u.Active && u.UserID.Equals(id));
            return await user.ToListAsync();
        }

        public async Task<LoginResponse> Login(LoginRequest loginRequest)
        {
            var user = context.Users.SingleOrDefault(user => user.Active && user.UserName == loginRequest.Username);
            if (user == null)
            {
                return null;
            }
            var passwordHash = HashingHelper.HashUsingPbkdf2(loginRequest.Password, user.PasswordSalt);
            if (user.Password != passwordHash)
            {
                return null;
            }
            var token = await Task.Run(() => TokenHelper.GenerateToken(user));
            return new LoginResponse { UserID=user.UserID, Username = user.UserName, Password =loginRequest.Password,FullName = user.FullName,Role=user.Role ,Token = token };
        }

        public async Task<bool> Update(string id, User user)
        {
            var users = context.Users.FirstOrDefault(o => o.UserID.Equals(id));
            user.PasswordSalt = "MTIzNDU2Nzg5MTIzNDU2Nw==";
            user.Password = HashingHelper.HashUsingPbkdf2(user.Password, user.PasswordSalt);
            context.Entry(users).CurrentValues.SetValues(user);
            context.SaveChanges();
            return true;
        }
    }
}
