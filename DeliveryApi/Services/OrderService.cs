﻿using DeliveryApi.Entites;
using DeliveryApi.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Services
{

    public class OrderService : IOrderService
    {
        private readonly DeliveryDbContext context;
        private readonly IHostingEnvironment _hostingEnvironment;

        public OrderService(DeliveryDbContext context, IHostingEnvironment hostingEnvironment)
        {
            this.context = context;
            _hostingEnvironment = hostingEnvironment;
        }


        public async Task<bool> Create(Order order)
        {
            var entrys = context.Orders.AddAsync(order);
            context.SaveChanges();
            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            context.Orders.Remove(entrys);
            context.SaveChanges();
            return true;
        }

        public async Task<bool> Ended(string id)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            if (entrys != null)
            {
                entrys.Status = "Ended";
                entrys.Result = "Success";
                context.Entry(entrys).CurrentValues.SetValues(entrys);
                context.SaveChanges();
            }
            return true;
        }

        public async Task<string> DownloadFile(IFormFile file)
        {
            string path = Path.Combine(_hostingEnvironment.WebRootPath, "ExcelFile/" + file.FileName);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            return "http://localhost:5000/ExcelFile/" + file.FileName;
        }

        public string Export()
        {
            string rootFolder = _hostingEnvironment.WebRootPath;
            string fileName = @"importOrder.xlsx";
            string result = "";

            FileInfo file = new FileInfo(Path.Combine(rootFolder, fileName));

            using (ExcelPackage package = new ExcelPackage(file))
            {

                List<Order> orders = context.Orders.ToList();

                if (package.Workbook.Worksheets.Count > 1)
                {
                    result = "Non empty worksheet";
                }
                else
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets["Order"];
                    int totalRows = orders.Count();

                    int i = 0;
                    for (int row = 5; row <= totalRows + 1; row++)
                    {
                        worksheet.Cells[row, 2].Value = orders[i].OrderID;
                        i++;
                    }

                    result = "GoodsLine list has been exported successfully";
                }
                package.Save();

            }

            return result;
        }

        public async Task<List<Order>> GetAll()
        {
            var entrys = context.Orders;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastID()
        {
            var orderID = context.Orders.OrderByDescending(x => x.OrderID).Take(1).Select(x => x.OrderID).ToList().FirstOrDefault();
            return orderID;
        }

        public async Task<List<Order>> GetUnderId(string id)
        {
            var entry = context.Orders.Where(u => u.OrderID.Equals(id));
            return await entry.ToListAsync();
        }


        public List<Order> Import()
        {
            string rootFolder = _hostingEnvironment.WebRootPath;
            string fileName = @"importOrder.xlsx";
            FileInfo file = new FileInfo(Path.Combine(rootFolder, "ExcelFile/" + fileName));


            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets["Order"];
                int totalRows = workSheet.Dimension.Rows;
                List<Order> orders = new List<Order>();
                for (int i = 5; i <= totalRows; i++)
                {
                    String selected = workSheet.Cells[i, 1].Text.Trim();
                    if (selected.Equals("x"))
                    {
                        orders.Add(new Order
                        {
                            OrderID = workSheet.Cells[i, 2].Text.Trim(),
                            RequiredDate = DateTime.Parse("01-01-2021"),
                            WarehouseID = workSheet.Cells[i, 6].Text.Trim(),
                            ShippingMethod = workSheet.Cells[i, 11].Text.Trim(),
                            OrderValue = Decimal.Parse(workSheet.Cells[i, 12].Text.Trim()),
                            BussinesStaffID = workSheet.Cells[i, 13].Text.Trim(),
                            Note = workSheet.Cells[i, 14].Text.Trim(),
                            ShippingType = "",
                            ShippedDate = DateTime.Parse("01-01-2021"),
                            Employee1 = "",
                            Employee2 = "",
                            OrderFee = 0,
                            StartTime = DateTime.Parse("01-01-2021"),
                            StopTime = DateTime.Parse("01-01-2021"),
                            Mass = 0,
                            Kilometers = 0,
                            Result = "",
                            Reason = "",
                            StartPoint = "",
                            EndPoint = "",
                            Status = "New",

                        });
                    }
                    //remove exist record
                    foreach (Order order in orders.ToList())
                    {
                        if (context.Orders.Any(u => u.OrderID.Equals(order.OrderID)))
                        {
                            orders.Remove(order);
                        }
                    }
                }
                context.Orders.AddRange(orders);
                context.SaveChanges();
                return orders;
            }
        }

        public async Task<List<Order>> Patch(string id)
        {
            var entry = context.Orders.Where(u => u.OrderID.Equals(id));
            return await entry.ToListAsync();
        }

        public async Task<bool> Update(string id, Order order)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            context.Entry(entrys).CurrentValues.SetValues(order);
            context.SaveChanges();
            return true;
        }

        public async Task<string> UploadFile(IFormFile file)
        {
            string path = Path.Combine(_hostingEnvironment.WebRootPath, "ExcelFile/" + file.FileName);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            return "http://localhost:5000/ExcelFile/" + file.FileName;
        }

        public async Task<bool> Cancel(string id,Order order)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
                order.Status = "Cancel";
                order.Result = "Failure";
                context.Entry(entrys).CurrentValues.SetValues(order);
               context.SaveChanges();
            return true;
        }
    }
}
