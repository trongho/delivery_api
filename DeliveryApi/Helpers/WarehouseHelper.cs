﻿using DeliveryApi.Entites;
using DeliveryApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Helpers
{
    public class WarehouseHelper
    {
        public static List<WarehouseModel> Covert(List<Warehouse> entrys)
        {
            var models = entrys.ConvertAll(sc => new WarehouseModel
            {
                WarehouseID=sc.WarehouseID,
                WarehouseName=sc.WarehouseName,
            });

            return models;
        }
    }
}
